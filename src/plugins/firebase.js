import firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyDkZDjM7vT8zcia2SMU0_WpZNax6meKX98',
  authDomain: 'badgestudio-c1bc2.firebaseapp.com',
  databaseURL: 'https://badgestudio-c1bc2.firebaseio.com',
  projectId: 'badgestudio-c1bc2',
  storageBucket: 'badgestudio-c1bc2.appspot.com',
  messagingSenderId: '34113913115'
}

export const fireApp = firebase.initializeApp(config)

export const AUTH = fireApp.auth()

export default ({ app, router, Vue }) => {
  Vue.prototype.$auth = AUTH
}

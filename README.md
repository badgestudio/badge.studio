# BadgeStudio

An application to make badges that are FAIR (Findable, Accessible, Interoperable & Re-usable)
Based on [Open Badges Standard](https://openbadges.org/) and building further on the [open Summer of Code](http://summerofcode.be) projects [Escobadges](http://escobadges.eu/) & [bSkilled](https://bskilled.today/#/). 

If you want to know more about Open Badges, you can visit the [badge.wiki](https://badge.wiki/) or subscribe to [badge news](http://badge.news/).

## Setup

Made with [Quasar-framework](https://quasar-framework.org/) & [Firebase](https://firebase.google.com) .

``` bash
# make sure you have vue-cli & quasar globally installed + Node.js >= 8.9.0 is required.
 
$ 'yarn global add vue-cli' (or 'npm install -g vue-cli')
$ 'yarn global add quasar-cli' (or 'npm install -g quasar-cli')
 
# install package dependencies
 
$ 'yarn install' (or 'npm install')
 
# serve with hot reload for development
 
$ 'quasar dev' (or 'quasar dev -m pwa' when developing a PWA)
 
# build for production
 
$ 'quasar build' (or 'quasar build -m pwa' when PWA mode)
 
```

## License

The MIT License (MIT)

Copyright (c) 2018 jeborsel - Open Knowledge Belgium

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import {https} from "firebase-functions"
import setupGraphQLServer from "./graphql/server"
import * as functions from "firebase-functions"
import admin from "firebase-admin"

admin.initializeApp(functions.config().firebase)

/* CF for Firebase with graphql-server-express */
const graphQLServer = setupGraphQLServer()

// https://us-central1-<project-name>.cloudfunctions.net/api
export const api = https.onRequest(graphQLServer)


import jws from "jws"
import fs from "fs"
import bakery from "openbadges-bakery"

const theassertion = {
  "uid": "abcdefghijklm1234567898765",
  "recipient": {
    "identity": "sha256$a1b2c3d4e5f6g7h8i9a1b2c3d4e5f6g7h8i9a1b2c3d4e5f6g7h8i9",
	"type": "email",
	"hashed": true
  },
  "badge": "http://issuersite.org/badge-class.json",
  "verify": {
    "url": "http://issuersite.org/public-key.pem",
	"type": "signed"
   },
   "issuedOn": 1403120715
}

const signature = jws.sign({
  header: {alg: 'rs256'},
  payload: theassertion,
  privateKey: fs.readFileSync(__dirname + '/private-key.pem')
})

exports.sign = https.onRequest((request, response) => {
  response.send(signature)
})

exports.assertions = https.onRequest((request, response) => {
response.send(admin.firestore().collection('assertions').doc('test')
           .get()
            .then(doc => {
                console.log('Got rule: ' + doc.data().name);
            }))
})